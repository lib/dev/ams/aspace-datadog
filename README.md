# ArchivesSpace DataDog Plugin

This ArchivesSpace plugin exports metrics and traces to DataDog.

It uses DataDog's `ddtrace` gem to auto-instrument the ArchivesSpace rails app.
